package composer

import (
	"encoding/json"

	"messaging-common-service/constant"
	"messaging-common-service/models"
	"os"
	"strconv"
)

//UserEventComposer -
type UserEventComposer struct {
}

//CreateEvent - create user event
func (uec UserEventComposer) CreateEvent(user models.UserAddModel) *models.AMQPMessage {
	message := models.AMQPMessage{}
	message.Event = constant.UserCreateEvent
	userMap := make(map[string]interface{})
	userMap["id"] = strconv.FormatInt(user.ID, 10)
	userMap["name"] = user.Name
	userMap["email"] = user.Email
	userMap["country"] = user.Country
	userMap["phone"] = user.Phone
	userMap["active"] = user.Active
	userMap["userName"] = user.UserName
	message.Sender = os.Getenv("app.name")
	message.Content, _ = json.Marshal(userMap)
	return &message
}
