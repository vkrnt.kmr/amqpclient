package constant

const (
	UserCreateEvent    string = "user.create"
	UserUpdateEvent    string = "user.update"
	UserDeleteEvent    string = "user.delete"
	ChannelCreateEvent string = "channel.create"
	ChannelUpdateEvent string = "channel.update"
	ChannelDeleteEvent string = "channel.delete"
	TagCreateEvent     string = "tag.create"
	CommentCreateEvent string = "comment.create"
	CommentDeleteEvent string = "comment.delete"
	CommentUpdateEvent string = "comment.update"
)
