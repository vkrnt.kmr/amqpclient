package event

import (
	"messaging-common-service/composer"
	"messaging-common-service/processor"
)

//UserEvent -
type UserEvent struct {
	Processor processor.UserEventProcessor
	Compose   composer.UserEventComposer
}
