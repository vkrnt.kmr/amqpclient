package main

import (
	"messaging-common-service/models"

	log "github.com/sirupsen/logrus"
)

//SampleReceiver -
func SampleReceiver() {
	forever := make(chan bool)

	session := New("host", "username", "password")
	amqpMessage := models.AMQPMessage{}
	amqpMessage.Content = "Testing"
	amqpMessage.Event = "sample"
	amqpMessage.Sender = "Test sender"
	amqpMessage.ToExchange = "test_exchange"

	connected := <-session.connected
	if connected {
		_, err := session.Listen("test_queue")
		if err != nil {
			log.Errorln(err)
		}
	}

	<-forever
}

//SampleSender  -
func SampleSender() {
	session := New("host", "username", "password")

	amqpMessage := models.AMQPMessage{}
	amqpMessage.Content = "Testing"
	amqpMessage.Event = "sample"
	amqpMessage.Sender = "Test sender"
	amqpMessage.ToExchange = "test_exchange"
	connected := <-session.connected

	if connected {
		err := session.Send(amqpMessage)
		if err != nil {
			log.Errorln(err)
		}
	}
}
func main() {

}
