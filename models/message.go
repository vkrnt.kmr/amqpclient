package models

//AMQPMessage object
type AMQPMessage struct {
	Event      string      `json:"event"`
	Content    interface{} `json:"content"`
	Sender     string      `json:"sender"`
	ToExchange string      `json:"toExchange"`
}
