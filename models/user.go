package models

// UserAddModel -
type UserAddModel struct {
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	UserName string `json:"userName"`
	Email    string `json:"email"`
	Country  string `json:"country"`
	Phone    string `json:"phone"`
	Active   bool   `json:"active"`
}

//UserUpdateModel - update
type UserUpdateModel struct {
	ID            int64  `json:"id"`
	Name          string `json:"name"`
	UserName      string `json:"userName"`
	Email         string `json:"email"`
	Country       string `json:"country"`
	Phone         string `json:"phone"`
	Active        bool   `json:"active"`
	Gender        string `json:"gender"`
	DOB           string `json:"dob"`
	ProfilePicURL string `json:"profilePicURL"`
	ThumbPicURL   string `json:"thumbPicURL"`
}

//UserDeleteModel - update
type UserDeleteModel struct {
	ID int64 `json:"id"`
}
