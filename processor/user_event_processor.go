package processor

import (
	"messaging-common-service/models"
	"strconv"
)

type UserEventProcessor struct {
}

//CreateEvent -
func (uep UserEventProcessor) CreateEvent(amqpMessage models.AMQPMessage) *models.UserAddModel {
	userMap := amqpMessage.Content.(map[string]interface{})
	user := models.UserAddModel{}
	user.ID, _ = strconv.ParseInt(userMap["id"].(string), 10, 64)
	user.Active = userMap["active"].(bool)

	if userMap["userName"] != nil {
		user.UserName = userMap["userName"].(string)
	}
	if userMap["country"] != nil {
		user.Country = userMap["country"].(string)
	}
	if userMap["email"] != nil {
		user.Email = userMap["email"].(string)
	}
	if userMap["name"] != nil {
		user.Name = userMap["name"].(string)
	}
	if userMap["phone"] != nil {
		user.Phone = userMap["phone"].(string)
	}
	return &user
}

//UpdateEvent -
func (uep UserEventProcessor) UpdateEvent(amqpMessage models.AMQPMessage) *models.UserUpdateModel {
    // TODO: USER UPDATE EVENT
    return nil
}

//DeleteEvent -
func (uep UserEventProcessor) DeleteEvent(amqpMessage models.AMQPMessage) *models.UserDeleteModel {
	user := models.UserDeleteModel{}
  user.ID,_ := strconv.ParseInt(message.Content.(string), 10, 64)
	return &user
}
